<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Test Response</title>
</head>

<body>
<%
String first = (String)request.getAttribute("first");
String last = (String)request.getAttribute("last");
%>
First name ${first}
Last name ${last}
</body>
</html>